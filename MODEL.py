import re
import math

from collections import defaultdict
from utils import if_file_exists_then_destroy

# MODEL
class MODEL(object):
    # transition, context, emit
    t = defaultdict(lambda: 0)
    c = defaultdict(lambda: 0)
    e = defaultdict(lambda: 0)

    def __init__(self, lines, sol, eol):
        self.sol = sol
        self.eol = eol
        for line in lines:
            pre = sol
            for wt in line:
                word, tag = wt.split("_")
                pre_tag = '{} {}'.format(pre, tag)
                tag_word = '{} {}'.format(tag, word)
                self.t[pre_tag] = self.t[pre_tag] + 1
                self.c[tag] = self.c[tag] + 1
                self.e[tag_word] = self.e[tag_word] + 1
                pre = tag
            pre_eol = '{} {}'.format(pre, eol)
            self.t[pre_eol] = self.t[pre_eol] + 1
            
            self.c[sol] = self.c[sol] + 1
            self.c[eol] = self.c[eol] + 1

    def print_to_file(self, file_dir, T, E, L):
        if_file_exists_then_destroy(file_dir)
        with open(file_dir, 'w', encoding='utf-8') as f:
            for key, value in self.t.items():
                pre = key.split(" ")[0]
                ret = value/self.c.get(pre, 1)
                f.write('{} {} {}\n'.format(T,key,ret))
            for key, value in self.e.items():
                pre = key.split(" ")[0]
                ree = L * ( value / self.c[pre] ) + ( 1 - L ) * ( 1 / (len(self.c)-1 ))
                f.write('{} {} {}\n'.format(E,key,ree))
        f.close()

    def print_possible_tags(self, file_dir):
        if_file_exists_then_destroy(file_dir)
        with open(file_dir, 'w', encoding='utf-8') as f:
            for line in self.c:
                f.write('{}\n'.format(line))
        f.close()

    def get_possible_tags(self, dataset):
        for line in dataset:
            self.c[line[0]] = self.c[line[0]] + 1

    def get_info_from_dataset(self, dataset, T, E):
        for line in dataset:
            if line[0] == T:
                self.t[line[1] + " " + line[2]] = line[3]
            elif line [0] == E:
                self.e[line[1] + " " + line[2]] = line[3]
            else:
                continue

    # Calculate prob
    def cal_prob(self, d, key, ie, l):
        if key in d:
            prob = float(d[key])
            if ie == 1:
                prob = self.lam * prob + ( 1 - self.lam ) / l
                # prob = self.lam * prob + ( 1 - self.lam ) / ( len(self.c) - 1)
        else:
            prob = self.min_prob       
        return -math.log(float(prob))

    # Viterbi - Forward step for n lines
    def viterbi(self, datatest, file_test_output, lam, min_prob):
        self.lam = lam
        self.min_prob = min_prob
        if_file_exists_then_destroy(file_test_output)
        with open(file_test_output, 'w') as f:
            for line in datatest:
                best_edge = self.viterbi_forward_step_line(line)
                result = self.viterbi_backward_step_line(line, best_edge)
                f.write('{}\n'.format(result))
        f.close()

    # Viterbi - Forward step for 1 line
    def viterbi_forward_step_line(self, line):
        best_score = dict()
        best_edge = dict()

        best_score['0 {}'.format(self.sol)] = 0
        best_edge['0 {}'.format(self.sol)] = None

        words = line.split()
        l = len(words)

        for i in range(0, l):
            for ktag in self.c.keys():
                i_ktag = '{} {}'.format(i, ktag)
                if i_ktag in best_score:
                    for ltag in self.c.keys():
                        ktag_ltag = '{} {}'.format(ktag, ltag)
                        ltag_ival = '{} {}'.format(ltag, words[i])

                        score = best_score[i_ktag] + \
                        self.cal_prob(self.t, ktag_ltag, 0, l) + \
                        self.cal_prob(self.e, ltag_ival, 1, l)

                        j_ltag = '{} {}'.format(i + 1, ltag)
                        if j_ltag not in best_score:
                            s = score + 1
                        else:
                            s = best_score[j_ltag]

                        if s > score:
                            best_score[j_ltag] = score
                            best_edge[j_ltag] = i_ktag

        for ktag in self.c.keys():
            len_ktag = '{} {}'.format(l, ktag)
            eol_wordl = '{} {}'.format(self.eol, words[l-1])
            lenp_eol = '{} {}'.format(l + 1, self.eol)
            ktag_eol = '{} {}'.format(ktag, self.eol)
            if len_ktag in best_score and ktag_eol in self.t:
                score = best_score[i_ktag] + \
                self.cal_prob(self.t, ktag_eol, 0, l) + \
                self.cal_prob(self.e, eol_wordl, 1, l)

                if lenp_eol not in best_score:
                    s = score + 1
                else:
                    s = best_score[lenp_eol]

                if s > score:
                    best_score[lenp_eol] = score
                    best_edge[lenp_eol] = len_ktag

        return best_edge

    def viterbi_backward_step_line(self, line, best_edge):
        tags = []
        words = line.split()
        next_edge = best_edge['{} {}'.format(len(words) + 1, self.eol)]

        while next_edge != "{} {}".format(0, self.sol):
            position, tag = next_edge.split(" ")
            tags.append(tag)
            next_edge = best_edge[next_edge]

        tags.reverse()

        sym = ''
        for tag in tags:
            sym = '{} {}'.format(sym, tag)

        sym = sym.strip()
        return sym
    