import re
import os
import json
import contextlib

from config import *
from pathlib import Path

# read file, tokenize to sentences
def read_sentences_from_file(file_dir):
    with open(file_dir, "r", encoding='utf-8') as f:
        return [re.split("\s+", line.rstrip('\n')) for line in f]

# read file, get lines
def get_lines_from_file(file_dir):
    result = []
    file = open(file_dir, 'r', encoding='utf-8')
    lines = file.read()
    for line in lines.split('\n'):
        result.append(line)
    file.close()
    while len(result[-1]) == 0:
        result = result[:-1]
    return result

# check file, if exist, destroy it
def if_file_exists_then_destroy(file_dir):
    with contextlib.suppress(FileNotFoundError):
        os.remove(file_dir)

# check if files exist
def check_if_files_exist(paths):
	flags = True
	for file_path in paths:
		flags = Path(file_path).is_file()
		if flags == False:
			print("Error: File ", file_path, " not exist!")
			break
	return flags		
