#!/usr/bin/env python

# Input is a test data without POS tags and an HMM tagger (from train-hmm.py)

# Test with wiki-en-test.norm
# Output is an answer file that includes POS tags for words in sentences.
# Save output text to my_answer.pos

# Measure the accuracy of your tagging with script gradepos.pl
# perl gradepos.pl data/wiki-en-test.pos my_answer.pos
# Report the accuracy

# Attention
# You need to load the HMM tagger before tagging
# You needs to implement Viterbi algorithm for decoding

from MODEL import MODEL
from config import *
import utils

import os
import subprocess

if __name__ == '__main__':
    files_exist = True
    file_train_output = FILE_TRAIN_OUTPUT
    file_test = FILE_TEST

    files_exist = utils.check_if_files_exist([FILE_TRAIN_OUTPUT, FILE_TEST])
    if files_exist == True:
        print("Reading Training Data output ...")
        possible_tags = utils.read_sentences_from_file(FILE_POSSIBLE_TAGS)
        dataset = utils.read_sentences_from_file(FILE_TRAIN_OUTPUT)

        model = MODEL([], START_OF_LINE, END_OF_LINE);
        model.get_possible_tags(possible_tags)
        model.get_info_from_dataset(dataset, PREFIX_TRANSITION, PREFIX_EMISSION)

        datatest = utils.get_lines_from_file(FILE_TEST)
        model.viterbi(datatest, FILE_TEST_OUTPUT, LAMDA, MIN_PROB)

        print("All finished without error!")
        
        # print("Checking, using Perl Script!")
        # subprocess.call(["gradepos.pl", "wiki-en-test.pos", "my_answer.pos"])
        # perl gradepos.pl wiki-en-test.pos my_answer.pos

    else:
        print("File(s) not found error, please recheck config file")