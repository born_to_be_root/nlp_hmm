# nlp_hmm

# Student
MSE0087 - DO VIET QUAN

# Dependencies
- python3 (3.6.6)
- Perl (for gradepos script)

# Configuration
Access config.py file to config it

`FILE_TRAIN` : training dataset

`FILE_TEST` : testing dataset

`FILE_POSSIBLE_TAGS` : store possible tags

`FILE_TRAIN_OUTPUT` : store model parameters

`FILE_TEST_OUTPUT` : output answer


`START_OF_LINE` : start of line symbol, don't use space or underscore for this

`END_OF_LINE` : end of line symbol, as above

`PREFIX_TRANSITION` : prefix for transition used for saving Model's parameters

`PREFIX_EMISSION` : prefix for emission used for saving Model's parameters

`LAMDA` : smoothing parameter, make it as close to 1 as possible for better accuracy

`MIN_PROB` : minimum probability if tag doesn't exist in Training Dataset. Make it a positive float, as close to 0 as possible.

# How to run

* Training process
```
python3 train-hmm.py to run training process
```

* Testing process
```
python3 train-hmm.py to run training process
```

# Thanks and best regards