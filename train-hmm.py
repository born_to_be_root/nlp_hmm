#!/usr/bin/env python

# Input of train-hmm is the training data and output are parameters of
# a first-order HMM tagger (i.e, transition and emission probabilities)

# Train with wiki-en-train.norm_pos

from MODEL import MODEL
from config import *

import utils

if __name__ == '__main__':
    files_exist = True
    file_train = FILE_TRAIN

    files_exist = utils.check_if_files_exist([file_train])
    if files_exist == True:
        print("Acquiring Training Data ...")
        dataset = utils.read_sentences_from_file(file_train)
        print("Training data acquired successfully from ", file_train)
        
        model = MODEL(dataset, START_OF_LINE, END_OF_LINE)
        model.print_to_file(FILE_TRAIN_OUTPUT, PREFIX_TRANSITION, PREFIX_EMISSION, LAMDA)
        model.print_possible_tags(FILE_POSSIBLE_TAGS)
        
        print("All finished without error!")
    else:
        print("File(s) not found error, please recheck config file")